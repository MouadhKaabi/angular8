import { Directive, HostBinding, HostListener } from '@angular/core';

@Directive({
  selector: '[appRinbow]'
})
export class RinbowDirective {


  tableau =
  [
    'blue',
    'red',
    'black',
    'yellow',
    'lightblue', 
    'green',
    'gold'
  ];

  @HostBinding ('style.borderColor') bc ;
  @HostBinding ('style.color') color ;

  constructor() { }


  @HostListener ('keypress') changerColor()
  {
    const index = Math.floor ( Math.random()*(this.tableau.length -1));

    this.bc = this.tableau[index];
    this.color = this.tableau[index];

  }

}
