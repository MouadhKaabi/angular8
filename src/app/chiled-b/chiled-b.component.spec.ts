import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChiledBComponent } from './chiled-b.component';

describe('ChiledBComponent', () => {
  let component: ChiledBComponent;
  let fixture: ComponentFixture<ChiledBComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChiledBComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChiledBComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
