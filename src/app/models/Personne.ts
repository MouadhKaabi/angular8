export class Personne 
{
    id :number; 
    nom: string ; 
    firstName : string; 
    age: number ;
    path:string;
    cin:string;
    job :string ;


    constructor (id=0,nom='',firstName='',age=0,path='',cin='',job='')
    {
        this.id = id ;
        this.nom=nom;
        this.firstName=firstName;
        this.age =age ;
        this.path = path ;
        this.cin=cin;
        this.job=job;
    }

}

