import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GrandChiledComponent } from './grand-chiled.component';

describe('GrandChiledComponent', () => {
  let component: GrandChiledComponent;
  let fixture: ComponentFixture<GrandChiledComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GrandChiledComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GrandChiledComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
