import { Component, OnInit, Input, Output,EventEmitter } from '@angular/core';
import { Personne } from 'src/app/models/Personne';


@Component({
  selector: 'app-listcv',
  templateUrl: './listcv.component.html',
  styleUrls: ['./listcv.component.css']
})
export class ListcvComponent implements OnInit {

  @Input() personnes:Personne[];
  @Output() selectedPersonne = new EventEmitter();
  constructor() { }

  ngOnInit() {
  }

  selectPersonne(selectedPersonne){
    this.selectedPersonne.emit(selectedPersonne);
  }

}
