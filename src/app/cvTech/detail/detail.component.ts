import { Component, OnInit } from '@angular/core';
import { CvService } from '../cv.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Personne } from 'src/app/models/Personne';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.css']
})
export class DetailComponent implements OnInit {
  personne : Personne ;
  constructor( 
    private cvService : CvService ,
    private activetedRoute : ActivatedRoute,
    private router : Router

  ) { }

  ngOnInit() {
    this.activetedRoute.params.subscribe(
      (parms)=>{
     this.cvService.getPersonneById(parms.id).subscribe(
       (personne)=>
       {
         this.personne = personne ;
       }
     );
      }
    );
  }

  deletePersonne()
  {
    this.cvService.deletePersonne(this.personne.id).subscribe(
      (response)=>
      {
        const link = ['cv'];
        this.router.navigate(link);
      },
      (error)=>
      {
        console.log(error);
      }  
    );
  }

}
