import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { CvService } from '../cv.service';
import { Router } from '@angular/router';



@Component({
  selector: 'app-add-cv',
  templateUrl: './add-cv.component.html',
  styleUrls: ['./add-cv.component.css']
})
export class AddCvComponent implements OnInit {

  nom = true;
  firstName = true;
  age = true ;
  cin = true;
  job = true;
  path= true;

  errorMessage ='' ;
  constructor(
    private cvService : CvService,
    private router : Router
    

  ) { }

  ngOnInit() {
  }

  AjouterCv(formulaire : NgForm)
  {
    console.log(formulaire.value);
    this.cvService.addPersonne(formulaire.value).subscribe(
      (response) => {
        const link =['cv'];
   
   this.router.navigate(link);
      },
      (error) => {
        this.errorMessage = `Probleme de connexion a serveur`;
       console.log(error) ;
      }
    )
    ;
    
  }

}
