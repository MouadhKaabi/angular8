import { Component, OnInit } from '@angular/core';
import { Personne } from 'src/app/models/Personne';
import { PremierService } from 'src/app/premier.service';
import { CvService } from '../cv.service';

@Component({
  selector: 'app-cv',
  templateUrl: './cv.component.html',
  styleUrls: ['./cv.component.css']
})
export class CvComponent implements OnInit {

  personnes : Personne [] ;
  selectedPersonne : Personne ;
  constructor(private cvService :CvService) {
    this.personnes=[
      new Personne(1,'Mouadh','Kaabi',26,'mouadh.jpg','123','etudiant'),
      new Personne(2,'ossma','bechir',25,'emna.png','123','etudiant')
    ];
   }

  ngOnInit() {

    this.cvService.getPersonne().subscribe(
      (personnes)=>
      {
        this.personnes = personnes;
      },

      (error) => {
        alert ('Probelem d\'aces a  l api ');
        console.log(error);
        this.personnes = this.cvService.getFakePersonne();

      }

    )  ;


   

  }
  selectPersonne(Personne)
  {
    this.selectedPersonne = Personne ;

  }

 

}
