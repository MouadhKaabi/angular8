import { Injectable } from '@angular/core';
import { Personne } from '../models/Personne';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CvService {

  personnes : Personne [];
  personne : Personne ;
 
  link ='http://localhost:3000/api/personnes' ;
  constructor(
    
    private http :HttpClient 
  ) {
   }
   getPersonne (): Observable < Personne []>
   {
     return this.http.get<Personne[]>(this.link);

   }
   getFakePersonne()
   {
     return this.personnes;
   }


   getPersonneById(id: number) :Observable<Personne>
   {
     return this.http.get<Personne>(this.link +`/${id}`);
   }


   addPersonne (personne : Personne ) : Observable <any>
   {
     return this.http.post(this.link , personne); 
   }

   deletePersonne(id: number)
   {
     return this.http.delete(this.link +`/${id}`);
   }

   findByName(firstName) : Observable<Personne [] >
   {
    const filter = `{"where":{"firstName":{"like":"%${firstName}%"}}}`;
    const params = new HttpParams().set('filter',filter)
    return this.http.get<Personne []>(this.link,{params});
   }
}
