import { Component, OnInit, Input } from '@angular/core';
import { Personne } from 'src/app/models/Personne';
import { EmbaucheService } from '../embauche.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-detailcv',
  templateUrl: './detailcv.component.html',
  styleUrls: ['./detailcv.component.css']
})
export class DetailcvComponent implements OnInit {

  @Input() personne :Personne ;
  constructor(private embaucheService:EmbaucheService,
              private router : Router
    ) { }

  ngOnInit() {
  }

  embaucher()
  {
    this.embaucheService.embaucher(this.personne);
  }

  moreInfo()
  {
    const link = ['cv',this.personne.id];
    this.router.navigate(link);
  }

}
