import { Component, OnInit, Input, Output,EventEmitter } from '@angular/core';
import { Personne } from 'src/app/models/Personne';


@Component({
  selector: 'app-itemcv',
  templateUrl: './itemcv.component.html',
  styleUrls: ['./itemcv.component.css']
})
export class ItemcvComponent implements OnInit {

  @Input () personne : Personne;
  @Output() selectedPersonne = new EventEmitter();


  constructor() { }

  ngOnInit() {
  }

  selectPersonne()
  {
    //emmtre un evenemnt et injecter un personne 

    this.selectedPersonne.emit(this.personne);
      
    
    

  }

}
