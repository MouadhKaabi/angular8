import { Component, OnInit } from '@angular/core';
import { Personne } from 'src/app/models/Personne';
import { CvService } from '../cv.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {

  chaine = '';
  searchResult : Personne [];
  constructor(
    private cvService : CvService ,
    private router : Router
  ) { }

  ngOnInit() {

    this.searchResult = [];
  }


  search()
  {
      const firstName = this.chaine.trim() ;
      if (firstName.length)
      {
        this.cvService.findByName(firstName).subscribe(
          (personnes)=> {
            console.log(personnes);
            this.searchResult = personnes ;
          }
        );
      } else {
        this.searchResult = [];
      }
     
  }

  selectPersonne(personne : Personne)
  {
   const link = ['cv', personne.id];

   this.router.navigate(link);

   this.searchResult = [];
   this.chaine = '';

  }

}
