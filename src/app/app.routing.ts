import { Routes, RouterModule } from '@angular/router';
import { CvComponent } from './cvTech/cv/cv.component';
import { PereComponent } from './pere/pere.component';
import { ClassComponent } from './directive/class/class.component';
import { StyleComponent } from './directive/style/style.component';
import { FilsComponent } from './fils/fils.component';
import { DetailComponent } from './cvTech/detail/detail.component';
import { AddCvComponent } from './cvTech/add-cv/add-cv.component';
import { DeleteCvComponent } from './cvTech/delete-cv/delete-cv.component';
import { ErrorComponent } from './error/error.component';
import { LoginComponent } from './login/login.component';


const APP_ROUTING: Routes =[
    {path:'cv', children:[
        {path:'', component : CvComponent},
        {path:'delete/:id', component : DeleteCvComponent},
        {path:'add', component : AddCvComponent},
        {path:':id', component : DetailComponent}
        
    ]},
    
    {path:'login', component: LoginComponent},
    {path:'pere/:default', component: PereComponent},
    {path:'class', component: ClassComponent},
    {path:'style', component: StyleComponent},
    {path:'fils', component: FilsComponent},
    {path:'**', component: ErrorComponent}

   


];

export const ROUTING =RouterModule.forRoot(APP_ROUTING);