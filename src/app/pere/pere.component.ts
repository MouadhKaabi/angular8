import { Component, OnInit } from '@angular/core';
import { PremierService } from '../premier.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-pere',
  templateUrl: './pere.component.html',
  styleUrls: ['./pere.component.css'],
  providers:[PremierService]

})
export class PereComponent implements OnInit {
  color ='yellow';
  constructor(private premiereService :PremierService,
              private router : Router,
              private activetedrout : ActivatedRoute
              
              ) { }

  ngOnInit() {
    this.activetedrout.params.subscribe
    (
      (parms) =>{
        console.log(parms);
        this.color = parms ['default'];

      }
    );
    
  }

  loggerMesdata()
  {
    this.premiereService.logger('test');
  }

  gotoCv()
  {
    const link = ['cv'];
    this.router.navigate(link);
  }

}
