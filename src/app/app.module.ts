import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ChiledAComponent } from './chiled-a/chiled-a.component';
import { ChiledBComponent } from './chiled-b/chiled-b.component';
import { GrandChiledComponent } from './grand-chiled/grand-chiled.component';
import { FormsModule } from '@angular/forms';
import { FilsComponent } from './fils/fils.component';
import { PereComponent } from './pere/pere.component';
import { CvComponent } from './cvTech/cv/cv.component';
import { ListcvComponent } from './cvTech/listcv/listcv.component';
import { ItemcvComponent } from './cvTech/itemcv/itemcv.component';
import { DetailcvComponent } from './cvTech/detailcv/detailcv.component';
import { StyleComponent } from './directive/style/style.component';
import { SComponent } from './directive/s/s.component';
import { ClassComponent } from './directive/class/class.component';
import { HighlightDirective } from './directive/highlight.directive';
import { RinbowDirective } from './directive/rinbow.directive';
import { DefaultimagePipe } from './cvTech/defaultimage.pipe';
import { EmbaucheComponent } from './cvTech/embauche/embauche.component';
import { ROUTING } from './app.routing';
import { HeaderComponent } from './header/header.component';
import { DetailComponent } from './cvTech/detail/detail.component';
import { AddCvComponent } from './cvTech/add-cv/add-cv.component';
import { DeleteCvComponent } from './cvTech/delete-cv/delete-cv.component';
import { ErrorComponent } from './error/error.component';
import { LoginComponent } from './login/login.component';
import { HttpComponent } from './http/http.component';
import {HttpClientModule, HttpParams} from '@angular/common/http';
import { SearchComponent } from './cvTech/search/search.component';


@NgModule({
  declarations: [
    AppComponent,
    ChiledAComponent,
    ChiledBComponent,
    GrandChiledComponent,
    FilsComponent,
    PereComponent,
    CvComponent,
    ListcvComponent,
    ItemcvComponent,
    DetailcvComponent,
    StyleComponent,
    SComponent,
    ClassComponent,
    HighlightDirective,
    RinbowDirective,
    DefaultimagePipe,
    EmbaucheComponent,
    HeaderComponent,
    DetailComponent,
    AddCvComponent,
    DeleteCvComponent,
    ErrorComponent,
    LoginComponent,
    HttpComponent,
    SearchComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,

    ROUTING
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
