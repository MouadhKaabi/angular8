import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChiledAComponent } from './chiled-a.component';

describe('ChiledAComponent', () => {
  let component: ChiledAComponent;
  let fixture: ComponentFixture<ChiledAComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChiledAComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChiledAComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
